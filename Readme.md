# Workshop03 - VHOSTs con Apache

## Agregar entradas al archivo de la máquina anfitriona

En la máquina afitriona nos dirijimos a la ruta `C:\Windows\System32\drivers\etc` en la máquina afitriona para agregar las nuevas 3 rutas

- lfts.isw811.xyz.conf
- lospatitos.com.conf
- elblogdevegazie.com.conf

![Host nuevos](images/newDomains.PNG)

## Copias los archivos de configuración de VHOSTs

Ingresamos a la carpeta confs que se ubica en la carpeta webserver, luego me

```bash
cd ~/ISW811/VMs/webserver
cd confs
cp vegazie.isw811.xyz.conf lfts.isw811.xyz.conf
cp vegazie.isw811.xyz.conf lospatitos.com.conf
cp vegazie.isw811.xyz.conf elblogdevegazie.com.conf
```

Luego reemplazamos las entradas `vegazie` por las que correspondan al dominio que queremos hospedar.

```bash
sed -i -e 's/vegazie/lfts/g' lfts.isw811.xyz.conf
sed -i -e 's/vegazie\.isw811\.xyz/lospatitos\.com/g' lospatitos.com.conf
sed -i -e 's/vegazie\.isw811\.xyz/elblogdevegazie\.com/g' elblogdevegazie.com.conf
```

Podemos comprobar que los archivos que hayan sido modificados correctamente con el comando `cat`

```bash 
cat lfts.isw811.xyz.conf
cat lospatitos.com.conf
cat elblogdevegazie.com.conf
```

## Crear el contenido para cada uno de los sitios

Creamos las carpetas donde se almacenara el código y los assets. Esto lo repetimos en las tres carpetas.

```bash
cd  ~/ISW811/VMs/webserver
cd sites
mkdir elblogdevegazie.com
mkdir lospatitos.com
mkdir lfts.isw811.xyz
```

Ahora accedemos a la carpeta de los `lospatitos.com` y creamos el archivo `index.html` y lo abrimos en VSCode. Esto lo repetimos en las tres carpetas.

```bash
cd  lospatitos.com
touch index.html
mkdir images
```

En el archivo `index.html` vamos a agregar el contenido de la página de pruebas para cada sitio.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Los Patitos</title>
  </head>
  <body>
    <img src="images/patitos.png" alt="patitos">
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu massa
        purus. 
      </p>
  </body>
</html>
```

## Copiar los archivos de configuración VHOSTs

Desde la máquina virtual debemos copiar los archivos de configuración de VHOSTs a la ruta de sitio disponibles de Apache2.

```bash
vagrant ssh
cd /vagrant/confs
sudo cp elblogdevegazie.com.conf /etc/apache2/sites-available/
sudo cp lfts.isw811.xyz.conf /etc/apache2/sites-available/
sudo cp lospatitos.com.conf /etc/apache2/sites-available/

sudo cp * /etc/apache2/sites-available/
sudo cp /vagrant/confs/* /etc/apache2/sites-available/
```

Una forma abreviada de copiar los archivos `conf` al directorio de sitios disponibles de Apache2, este comando lo puede lanzar desde cualquier ubicación de la máquina virtual:

```bash
sudo cp /vagrant/confs/* /etc/apache2/sites-available/
```

Seguidamente utilizamos el comando `sudo a2ensite` para habilitar los archivos `confs` y cada vez que ejecutemes el comando `a2ensite` debemos verificar si la sintaxis de los archivos estan correctos.

```bash
 sudo a2ensite lospatitos.com.conf
 sudo apache2ctl -t
 sudo a2ensite elblogdevegazie.com.conf 
 sudo apache2ctl -t
 sudo a2ensite lfts.isw811.xyz.conf
 sudo apache2ctl -t
```

Luego reiniciamos el Apache2 para que las páginas habilitadas se muestren

```bash
 sudo systemctl reload apache2
```

## Páginas de pruebas funcionando

Estas serían las páginas habilitadas:

Primera página de prueba que se habilito en el Workshop02

Página de pruebas:

![Página de prueba](images/dommiePages.PNG)

Estas son las 3 páginas que se agregaron en este Workshop03

Blog de VegaZie:

![Blog de Vegazie](images/blodevegazie.PNG)

Los Patitos:

![Los Patitos](images/losPatitos.PNG)

Laravel from the Scratch:

![Laravel From The Scratch](images/lfts.PNG)

## Crear proyecto laravel

```bash
cd ~

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
rm composer-setup.php

sudo mkdir -p /opt/composer/
sudo mv composer.phar /opt/composer/
sudo ln -s /opt/composer/composer.phar /usr/bin/composer

cd /vagrant/sites
rm -r lfts.isw811.xyz
composer create-project laravel/laravel:8.6.12 lfts.isw811.xyz
```

Cambiamos el archivo de configuración `lfts.isw.xyz.conf` este se encuentra en la ruta ``

```bash
<VirtualHost *:80>
ServerAdmin webmaster@lfts.isw811.xyz
ServerName lfts.isw811.xyz

DirectoryIndex index.php index.html
  DocumentRoot /home/vagrant/sites/lfts.isw811.xyz/public

  <Directory /home/vagrant/sites/lfts.isw811.xyz/public>
  DirectoryIndex index.php index.html
  AllowOverride All
  Require all granted
  </Directory>
  
    ErrorLog ${APACHE_LOG_DIR}/lfts.isw811.xyz.error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/lfts.isw811.xyz.access.log combined
 </VirtualHost>
```

Finalmente debemos volver a copiar el archivo `lfts.isw811.xyz.conf` a la ruta `/etc/apache2/sites-available/`, seguidamente verificamos su sintaxis y reiniciamos apache.

```bash
cd /vagrant/confs
sudo cp lfts.isw811.xyz.conf /etc/apache2/sites-available/
sudo apache2ctl -t
sudo systemctl reload apache2
```

Una vez terminados estos pasos nos quedaría habilitado nuestro sitio laravel como se muestra en la siguiente imagen:

![Sitio Laravel](images/sitioLaravel.PNG)
